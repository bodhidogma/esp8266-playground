/*
 */

#include <mymain.h>
#include <stdio.h>

#include "dev_led.hpp"
#include "stm_console.hpp"

// global objects

// StmConsole console(&huart1, false);
StmConsole console(NULL, true);
DevLED led0(LED0_GPIO_Port, LED0_Pin);
DevLED led1(LED1_GPIO_Port, LED1_Pin);

static uint32_t sys_now_ms = 0;

/**
 *
 */
void main_pre_loop(void){

};

/**
 *
 */
void main_loop(void) {
  uint32_t last_now_ms_ = 0;

  // uint8_t buffer[] = "Hello, World!\r\n";
  // CDC_Transmit_FS(buffer, sizeof(buffer));

  // disable stdio buffering
  setbuf(stdout, NULL);

  // init global classes
  console.Initialize();
  led0.SetPattern(DevLED::BLINK1);

  // HAL_GPIO_WritePin(GPIOB, LED1_Pin|LED0_Pin, GPIO_PIN_RESET);	// RESET
  // HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
  // HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);

  while (1) {
    sys_now_ms = millis();

    if (sys_now_ms - last_now_ms_ > 1000) {
      last_now_ms_ = sys_now_ms;

      // HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
      //  fprintf( stderr, ".");
      // printf(".");
      // console.Send(".", 1);
      // console.Send("some data --", 12);
      // console.Send("++ more bytes! ", 15);
    }

    console.Update();
    led0.Update();
    led1.Update();
  }
}
