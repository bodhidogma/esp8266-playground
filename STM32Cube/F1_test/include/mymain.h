/**
 * 
*/

#ifndef __MYMAIN_H__
#define __MYMAIN_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include "usart.h"
//#include "usbd_cdc_if.h"

#include <stdlib.h>
#include <inttypes.h>

/*
 *  Primary entry points from main.c
 */
void main_pre_loop();
void main_loop();

#ifdef __cplusplus
}
#endif

// arduino like macros
#define millis()	HAL_GetTick()		// get current ms elapsed
#define delay(x)	HAL_Delay(x)		// delay ms

#define CON_PRINTf  printf
#define NL  "\r\n"

#endif