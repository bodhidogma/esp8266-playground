env = DefaultEnvironment()

# The HAL sources must be able to include stm32h7xx_hal_conf.h
env.Append(CPPPATH=["$PROJECT_DIR/lib/cubemx_generated/Inc"])

# Set filename of final .elf & .bin
#env.Replace(PROGNAME="airwolf_ctl_bootloader")