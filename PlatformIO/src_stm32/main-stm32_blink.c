#if 1
// #define F3 1
// #define H7 1

#include <stdio.h>
#include <sys/unistd.h>  // STDOUT_FILENO, STDERR_FILENO

#if F0
#include "stm32f0xx_hal.h"
#elif F1
#include "stm32f1xx_hal.h"
#elif F2
#include "stm32f2xx_hal.h"
#elif F3
#include "stm32f3xx_hal.h"
#elif F4
#include "stm32f4xx_hal.h"
#elif F7
#include "stm32f7xx_hal.h"
#elif L0
#include "stm32l0xx_hal.h"
#elif L1
#include "stm32l1xx_hal.h"
#elif L4
#include "stm32l4xx_hal.h"
#elif H7
#include "stm32h7xx_hal.h"
#else
#error "Unsupported STM32 Family"
#endif

#if F1
#define LED0_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define LED_GPIO_CLK_ENABLE() __HAL_RCC_GPIOC_CLK_ENABLE()

#elif F3
#define LED0_Pin GPIO_PIN_4  // led 0
#define LED1_Pin GPIO_PIN_5  // led 1
#define LED_GPIO_Port GPIOB
#define LED_GPIO_CLK_ENABLE() __HAL_RCC_GPIOB_CLK_ENABLE()
// #define LED0_Pin                               GPIO_PIN_0 // beeper
// #define LED_GPIO_Port                          GPIOA
// #define LED_GPIO_CLK_ENABLE()                  __HAL_RCC_GPIOA_CLK_ENABLE()

#elif H7
// #define LED_Pin                                GPIO_PIN_8
#define LED_Pin GPIO_PIN_14
#define LED_GPIO_Port GPIOB
#define LED_GPIO_CLK_ENABLE() __HAL_RCC_GPIOB_CLK_ENABLE()

#endif

UART_HandleTypeDef huart1;

void Error_Handler(void);

#if 1
#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif

PUTCHAR_PROTOTYPE
{
#if 1
  HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, HAL_MAX_DELAY);
#else
  CDC_Transmit_FS((uint8_t *)&ch, 1);
#endif
  return ch;
}
#endif

void MX_GPIO_Init(void) {
  LED_GPIO_CLK_ENABLE();

  GPIO_InitTypeDef GPIO_InitStruct;

  GPIO_InitStruct.Pin = LED0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

#if LED1_Pin
  GPIO_InitStruct.Pin = LED1_Pin;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);
#endif
}

void MX_USART1_UART_Init(void) {
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK) {
    Error_Handler();
  }
}

int main(void) {
  uint8_t message[] = {"hello world..\r\n"};

  HAL_Init();

  MX_GPIO_Init();
  MX_USART1_UART_Init();

  while (1) {
#ifdef LED1_Pin
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED1_Pin);
#endif
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED0_Pin);
    HAL_Delay(1000);
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED0_Pin);
    HAL_Delay(250);
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED0_Pin);
    HAL_Delay(250);

    //printf("hello");
    //HAL_UART_Transmit(&huart1, message, 16, HAL_MAX_DELAY);
  }
}

void SysTick_Handler(void) { HAL_IncTick(); }

void NMI_Handler(void) {}

void HardFault_Handler(void) {
  while (1) {
  }
}

void MemManage_Handler(void) {
  while (1) {
  }
}

void BusFault_Handler(void) {
  while (1) {
  }
}

void UsageFault_Handler(void) {
  while (1) {
  }
}

void SVC_Handler(void) {}

void DebugMon_Handler(void) {}

void PendSV_Handler(void) {}

void Error_Handler(void) {
  __disable_irq();
  while (1) {
  }
}

#endif