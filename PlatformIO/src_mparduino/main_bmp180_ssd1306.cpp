#if 0 // build on not

#include <Arduino.h>
#include <Adafruit_BMP085.h>

/*************************************************** 
  This is an example for the BMP085 Barometric Pressure & Temp Sensor

  Designed specifically to work with the Adafruit BMP085 Breakout 
  ----> https://www.adafruit.com/products/391

  These pressure and temperature sensors use I2C to communicate, 2 pins
  are required to interface
  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

// Connect VCC of the BMP085 sensor to 3.3V (NOT 5.0V!)
// Connect GND to Ground
// Connect SCL to i2c clock - on '168/'328 Arduino Uno/Duemilanove/etc thats Analog 5
// Connect SDA to i2c data - on '168/'328 Arduino Uno/Duemilanove/etc thats Analog 4
// EOC is not used, it signifies an end of conversion

// XCLR is a reset pin, also not used here
// Simple I2C test for 128x32 oled.
// Use smaller faster AvrI2c class in place of Wire.
// Edit AVRI2C_FASTMODE in SSD1306Ascii.h to change the default I2C frequency.
//
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

// 0X3C+SA0 - 0x3C or 0x3D
#define I2C_ADDRESS 0x3C

// Define proper RST_PIN if required.
#define RST_PIN -1

SSD1306AsciiAvrI2c oled;
Adafruit_BMP085 bmp;
  
void setup() {
#if RST_PIN >= 0
  oled.begin(&Adafruit128x32, I2C_ADDRESS, RST_PIN);
#else // RST_PIN >= 0
  oled.begin(&Adafruit128x32, I2C_ADDRESS);
#endif // RST_PIN >= 0
  // Call oled.setI2cClock(frequency) to change from the default frequency.

  oled.setFont(Adafruit5x7);
  oled.clear();
  // first row
  oled.println("set1X test");
  uint32_t m = micros();

  // second row
  oled.set2X();
  oled.println("set2X test");

  Serial.begin(115200);
  delay(1000);

  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    while (1) {}
  }

  Serial.println("BMP085 found");

    Serial.print("Temperature (*C),");
//    Serial.print("Pressure (pA),");
    Serial.print("Altitude (m),");
//    Serial.print("Calc Sealevel Pressure (pA),");
    Serial.print("Real altitude (m)");
    Serial.println();

  // third row
  oled.set1X();
  oled.print("micros: ");
  oled.print(micros() - m);

  #if INCLUDE_SCROLLING == 0
  #error INCLUDE_SCROLLING must be non-zero.  Edit SSD1306Ascii.h
  #endif //  INCLUDE_SCROLLING
  //oled.setScrollMode(SCROLL_MODE_AUTO);
  oled.set2X();
  oled.clear();
}

#define DELAY_SEC     10
#define HISTORY_MIN   10

int state = 0;
float hist_temp[2] = {0};
long  hist_mb[2] = {0};
int hist_sel = 0;
int hist_cnt = 0;
  
void loop() {

  char buff[128] = {0};
  char trend_temp = ' ', trend_mb= ' ', temp_type = 'C';
  int tmpI, tmpI2;
  float tempVal, tmpF;
  long press_mb = trunc(bmp.readPressure() / 100);

  tempVal = bmp.readTemperature();

  if (hist_cnt == 0) {
    hist_temp[hist_sel] = tempVal;
    hist_mb[hist_sel] = press_mb;
    hist_sel ^= 1;
  }

  if (hist_temp[hist_sel]) {
    if (tempVal < (hist_temp[hist_sel]-0.1))
      trend_temp = 'v';
    else if (tempVal > (hist_temp[hist_sel]+0.1))
      trend_temp = '^';
    else
      trend_temp = '~';

    if (press_mb < (hist_mb[hist_sel]))
      trend_mb = 'v';
    else if (press_mb > (hist_mb[hist_sel]))
      trend_mb = '^';
    else
      trend_mb = '~';
  }

  if (state) {
    tempVal = (tempVal * 9/5) + 32;
    temp_type = 'F';
  }

  tmpI = tempVal;
  tmpF = tempVal-tmpI;
  tmpI2 = trunc(tmpF * 100);

  sprintf(buff, "%c%d.%02d `%c %c",
    trend_temp, tmpI, tmpI2, temp_type, (state ? '-':'|')
  );

  oled.home();
  //oled.clear();
  //oled.set2X();
  oled.println(buff);
  //oled.println("°C");

  sprintf(buff, "%c %ld mB  ",
      trend_mb,
      press_mb
    );
  oled.println(buff);
  state = !state;

  //
  if ((++hist_cnt) > ((HISTORY_MIN*60) / DELAY_SEC)) {
    hist_cnt = 0;
  }

  sprintf(buff, "%d\t %ld\t cnt:%d/%d hs:%d\t %c ht0:%d ht1:%d, %c hm0:%ld hm1:%ld",
    (int)(tempVal*100), press_mb, hist_cnt, ((HISTORY_MIN*60) / DELAY_SEC), hist_sel,
    trend_temp, (int)(hist_temp[0]*100), (int)(hist_temp[1]*100),
    trend_mb, hist_mb[0], hist_mb[1]
    );
  Serial.print(buff);

  //Serial.print(bmp.readTemperature()); Serial.print(", ");
  //Serial.print(bmp.readPressure()); Serial.print(", ");
  
  // Calculate altitude assuming 'standard' barometric
  // pressure of 1013.25 millibar = 101325 Pascal
  //Serial.print(bmp.readAltitude()); Serial.print(", ");

  //Serial.print(bmp.readSealevelPressure()); Serial.print(", ");

// you can get a more precise measurement of altitude
// if you know the current sea level pressure which will
// vary with weather and such. If it is 1015 millibars
// that is equal to 101500 Pascals.
  //Serial.print(bmp.readAltitude(101500)); //Serial.print(", ");
  Serial.println();

  delay(DELAY_SEC * 1000);

}

#endif