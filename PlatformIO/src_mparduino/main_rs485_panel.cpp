#if 0
/*
MODBUS / RS485 testing

    https://drive.google.com/file/d/1VS2JYvjreUWsw3MOgpfzmrMsCzUD_mnA/view

    DEV ID | Function | ADDRESS | DATA | CRC
     1B    |  1B      |  2B     |  nB  |  2B

    F: x03 = read address
    F: x06 = set address data
    F: x83 = read instruction error return
    F: x86 = set specified address error return

    * read button status
    ->: 02 03 10 0B 01 <CRC>
    <-: 02 03 00 02 xx xx <CRC> (xx xx == button pressed)

    ---
    sequence
    setup()
        set panel config: slave-send
        > 02 06 1003 0004

        set panel config: 15s light off delay?
        > 02 06 1003 xxxx

    loop()
        if (rx-button-msg) > 02 03 0002 XXXX
            set-led for pressed button > 02 06 1008 XXXX
            poll to check for release > 02 03 131? 0001

*/

#include <Arduino.h>

#if defined(__SAMD21G18A__)
#define Serial SerialUSB

#define Serial485 Serial1
#define RS485_RTS 2
// #define MASTER_TX 1
// #define MASTER_RX 0

#elif defined(ESP32)

#define Serial485 Serial2 // RS485 serial port
#define RS485_RTS 4       // RS485 en/rts

// #define MASTER_TX 17
// #define MASTER_RX 16

#else // other platforms

#endif

char sBuff[128];
uint16_t crc_chk_value(char *data_value, size_t length);

// #define DEBUG_RS485_RX
// #define DEBUG_RS485_TX

#define MSG_DEF_LEN 8   // default message length
#define MSG_ACTIVE_RX 1 // message RX is active
#define MSG_ACTIVE_TX 2 // message TX is active

#define MSG_RX 0 // RX index
#define MSG_TX 1 // TX index

#define RX_MSG_TIMOUT_MS 250 // max rx delay before msg fail
#define CMD_DELAY_MS 100     // spec= 100ms
#define BTN_LAMP_DELAY 500   // how long to leave the lamp on after button press
#define BTN_POLL_MS 200      // polling frequency when holding button down (min 150)

#define PANEL_CMD_GET 0x03
#define PANEL_CMD_SET 0x06
#define PANEL_CMD_GET_ERR 0x83
#define PANEL_CMD_SET_ERR 0x86

#define PANEL_REG_ADDR 0x1000
#define PANEL_REG_CONFIG 0x1003
#define PANEL_REG_LED 0x1008
#define PANEL_REG_BUTTON 0x100B
#define PANEL_REG_BTNSTATE 0x1310

#define PANEL_CFGBIT_ANTILED 1 << 0
#define PANEL_CFGBIT_SENDBTN 1 << 2  // master / slave polling
#define PANEL_CFGBIT_DELAYOFF 1 << 4 // enable light on, delayed turn off (5s)
#define PANEL_CFGBIT_BACKGND 1 << 6

#define byte2_val(x) (x[0] | (x[1] << 8))

/**
 * @brief G2 Message packet structure
 *
 */
typedef struct
{
    uint8_t id;
    uint8_t cmd;
    uint8_t adln[2]; // addr or data length
    uint8_t data[2]; // could be longer depending..
    union
    {
        uint8_t crc[2];    // last 2 bytes == CRC
        uint8_t xdata[14]; //
    };
} G2Message;

/**
 * @brief Manage RX/TX of G2 Message packets
 *
 */
class PanelMsg
{
private:
public:
    G2Message g2Buff[2]; // RX/TX Buffer
    char *pTRx[2];       // ptr to RX/TX buffer

    uint16_t msgLen[2];        // current RX/TX message length
    uint16_t calcCRC[2];       // calculated RX/TX CRC
    unsigned long lastRxTime;  // tracking rx timeout
    unsigned long lastCmdTime; // tracking tx delay (100ms) between cmds
    uint8_t rtxActive;         // RX / TX active 1 == rx, 2 == tx

    PanelMsg()
    {
        pinMode(RS485_RTS, OUTPUT);
        clear();
    }
    ~PanelMsg() {}

    int trxMessage(HardwareSerial &rSerial);

    int rxMsgData(HardwareSerial &rSerial);
    int txMsgData(HardwareSerial &rSerial);
    int txMsgDataWait(HardwareSerial &rSerial);

    void *setAddrLen(const char *buff, size_t len)
    {
        if (len < 3)
            return memcpy(g2Buff[0].adln, buff, len);
        else
            return 0;
    }
    void *setData(const char *buff, size_t len)
    {
        if (len < 3)
            return memcpy(g2Buff[0].data, buff, len);
        else
            return 0;
    }

    int setTxCmdData(uint8_t cmd, uint16_t cfg_addr, uint16_t cfg_val);

    int crc_calc(bool tx_mode = false);
    void clear()
    {
        clear_rx();
        clear_tx();
    }
    void clear_rx();
    void clear_tx();
};

/**
 * @brief reset our message state
 *
 */
void PanelMsg::clear_rx()
{
    // reset message state
    this->pTRx[MSG_RX] = (char *)&this->g2Buff[MSG_RX];
    memset(this->pTRx[MSG_RX], 0, sizeof(G2Message));
    this->msgLen[MSG_RX] = 8;  // default length
    this->calcCRC[MSG_RX] = 0; // reset calculated CRC
    this->lastRxTime = 0; // reset our rx timer
    this->rtxActive = 0;  // RX / TX mode inactive (no active RX | TX)

    digitalWrite(RS485_RTS, LOW); // RX mode enabled
}

void PanelMsg::clear_tx()
{
    clear_rx();

    // reset message state
    this->pTRx[MSG_TX] = (char *)&this->g2Buff[MSG_TX];
    memset(this->pTRx[MSG_TX], 0, sizeof(G2Message));
    this->msgLen[MSG_TX] = 8;  // default length
    this->calcCRC[MSG_TX] = 0; // reset calculated CRC
    // this->lastRxTime = 0; // reset our rx timer
    this->rtxActive = 0; // RX / TX mode inactive (no active RX | TX)
}

/**
 * @brief TX a message packet over some serial link
 *
 * @param rSerial
 * @return int 0 == msg tx okay
 *
 */
int PanelMsg::txMsgDataWait(HardwareSerial &rSerial)
{
    // active RX? don't try and TX
    if (this->rtxActive == MSG_ACTIVE_RX)
    {
        Serial.print(" tx/active: ");
        Serial.println(this->rtxActive);
        return -1;
    }

    // no active state? set active TX
    if (this->rtxActive == 0)
        this->rtxActive = MSG_ACTIVE_TX;

    // need to wait for delay before sending next packet
    // Serial.print(">");
    // Serial.print(millis() - lastCmdTime);
    if (this->rtxActive == MSG_ACTIVE_TX && lastCmdTime)
    {
        while ((millis() - lastCmdTime) <= CMD_DELAY_MS)
            delay(1);
        lastCmdTime = 0;
    }
    // Serial.print(">");

    return txMsgData(rSerial);
}

int PanelMsg::txMsgData(HardwareSerial &rSerial)
{
    int ret = 0;

    // not ready to TX? return
    if (this->rtxActive != MSG_ACTIVE_TX)
        return 0;

        // wait min time betewen cmds
#if 0
    if (lastCmdTime)
    {
        while ((millis() - lastCmdTime) <= CMD_DELAY_MS)
            delay(1);
    }
#else
    // not enough time has elapsed since last cmd, don't TX
    if (lastCmdTime && ((millis() - lastCmdTime)) <= CMD_DELAY_MS)
        return 0;
#endif
    this->crc_calc(true);

#ifdef DEBUG_RS485_TX
    char *p = (char *)&this->g2Buff[MSG_TX];
    for (int i = 0; i < this->msgLen[MSG_TX]; i++)
    {
        sprintf(sBuff, ".%02x", *p++);
        Serial.print(sBuff);
    }
    Serial.println();
#endif

    digitalWrite(RS485_RTS, HIGH); // TX mode enabled
    // delay(1);

    ret = rSerial.write((const char *)&this->g2Buff[MSG_TX], this->msgLen[MSG_TX]); // send our cmd message
    rSerial.flush();                                                      // wait for send to complete

    for (int i = 0; i < 2; i++) // wait a bit to ensure all bytes went out
        delay(1);
    digitalWrite(RS485_RTS, LOW); // RX mode enabled

    lastCmdTime = millis(); // track last cmd time
    this->rtxActive = 0;    // active mode is cleared

#if 1
    if (ret != this->msgLen[MSG_TX])
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }
#endif

    return (ret != this->msgLen[MSG_TX]) ? -1 : ret;
}

/**
 * @brief RX some message packet from a serial link
 *
 * @param rSerial
 * @return int 0 == incomplete, else CRC value
 *
 * Call repeatedly until a complete message has been recived
 * If a CRC error or timeout occurs, message is dumped and new RX must take place
 */
int PanelMsg::rxMsgData(HardwareSerial &rSerial)
{
    int ret = 0;
    uint8_t cur_len;

    if (rSerial.available() > 0)
    {
        // active TX? don't try and RX
        if (this->rtxActive == MSG_ACTIVE_TX)
        {
            Serial.print(" rx/active: ");
            Serial.println(this->rtxActive);
            return -1;
        }

        this->rtxActive = MSG_ACTIVE_RX; // RX data, set active mode

        while (rSerial.available())
        {
            char c = rSerial.read(); // read the data from serial.

            this->lastRxTime = millis(); // record our last bye rx'd time (for timeout)
            *this->pTRx[0]++ = c;        // append char to end of buffer

            cur_len = (this->pTRx[MSG_RX] - (char *)&this->g2Buff[MSG_RX]);

#ifdef DEBUG_RS485_RX
            sprintf(sBuff, ".%02x", c);
            // sprintf(sBuff, ".%02x(%d)", c, cur_len);
            Serial.print(sBuff);
#endif
            // reached our message / packet size
            if (cur_len >= (this->msgLen[MSG_RX]) || (cur_len >= sizeof(G2Message)))
            {
                this->lastRxTime = 0;          // reset our rx timer
                this->rtxActive = 0;           // RX / TX mode inactive (no active RX | TX)
                this->lastCmdTime = millis();  // track last cmd time
                digitalWrite(RS485_RTS, HIGH); // RX mode disabled

                if (!(ret = this->crc_calc()))
                    this->clear_rx();
                break;
            }
        }
    }
    else if (this->lastRxTime)
    {
        // have we waited too long since last byte rx'd?
        if ((millis() - this->lastRxTime) > RX_MSG_TIMOUT_MS)
        {
#ifdef DEBUG_RS485_RX
            Serial.println("~t/o~");
#endif
            this->lastCmdTime = millis(); // track last cmd time
            this->clear_rx();             // reset our messsage - did not complete?
        }
    }
    return ret;
}

int PanelMsg::trxMessage(HardwareSerial &rSerial)
{
    int ret = 0;

    ret = rxMsgData(rSerial);
    return ret;
}

int PanelMsg::setTxCmdData(uint8_t cmd, uint16_t cfg_addr, uint16_t cfg_val)
{
    int ret = 0;
    // dont update buffer if in middle of TX / RX
    if (this->rtxActive)
    {
        Serial.print(" cmd/active: ");
        Serial.println(this->rtxActive);
        return -1;
    }
    // Serial.println("txCmd");
    // this->clear();                   // clear buffer roady for loading
    this->clear_tx();                // clear buffer roady for loading
    this->rtxActive = MSG_ACTIVE_TX; // set active mode ready to send

    this->g2Buff[MSG_TX].id = 0x02;
    this->g2Buff[MSG_TX].cmd = cmd; // 3 = read, 6 = write

    // 1003 = cfg, 1008 = LED, 100B = BTN, 1310 = btn status
    this->g2Buff[MSG_TX].adln[0] = (cfg_addr & 0xFF00) >> 8;
    this->g2Buff[MSG_TX].adln[1] = cfg_addr & 0x00FF;

    this->g2Buff[MSG_TX].data[0] = (cfg_val & 0xFF00) >> 8;
    this->g2Buff[MSG_TX].data[1] = cfg_val & 0x00FF;

    return ret;
}

/**
 * @brief Calculate message CRC value and compare or set to message
 *
 * @param set_crc
 * @return int 0 or CRC value
 */
int PanelMsg::crc_calc(bool tx_mode)
{
    int ret = 0;
    uint16_t crc_msg = 0;
    G2Message *g2Ptr;

    uint8_t trx = tx_mode;

    // if (tx_mode)
    g2Ptr = &this->g2Buff[trx];

    this->calcCRC[trx] = crc_chk_value((char *)g2Ptr, this->msgLen[trx] - 2);
    // perform msg CRC check
    if (!tx_mode)
    {
        // crc_msg = this->g2Buff.crc[0] | (this->g2Buff.crc[1] << 8);
        crc_msg = byte2_val(g2Ptr->crc);
        ret = crc_msg;
    }
    // update the msg CRC with calc'd value
    else
    {
        g2Ptr->crc[1] = this->calcCRC[trx] >> 8;
        g2Ptr->crc[0] = this->calcCRC[trx] & 0xFF;
        ret = this->calcCRC[0];
    }
#if defined(DEBUG_RS485_RX) && defined(DEBUG_RS485_TX)
    sprintf(sBuff, " ~ id: %x cmd: %x adln: %02x%02x dat: %02x%02x ", this->g2Buff.id, this->g2Buff.cmd,
            this->g2Buff.adln[0], this->g2Buff.adln[1],
            this->g2Buff.data[0], this->g2Buff.data[1]);
    Serial.println(sBuff);
    if (set_crc)
    {
        sprintf(sBuff, "CRC:(%04x)", this->calcCRC);
        Serial.print(sBuff);
    }
#endif
    // CRC check / failure
    if (!tx_mode && (this->calcCRC[trx] != crc_msg))
    {
#ifdef DEBUG_RS485_RX
        sprintf(sBuff, "CRC:(%04x != %04x)", this->calcCRC[trx], crc_msg);
        Serial.print(sBuff);
#endif
        return 0;
    }
    return ret;
}

enum _PanelState
{
    RX_BUTTON,
    TX_STATUS,
    MAX_STATE
};

/**
 * @brief Manage a G2 6 button panel
 *
 */
class PanelManager
{
private:
public:
    PanelMsg trxMsg;

    uint8_t btnState[6];
    unsigned long btnTime[6];

    uint8_t btnHoldNum;

    PanelManager() {}
    ~PanelManager() {}

    int buttonPress();
    int buttonUpdate();

    void setup();
    int loop();
};

int PanelManager::buttonPress()
{
    // record button press
    // uint16_t addr = byte2_val(rxMsg.g2Buff.adln);
    // btn_val - (btn_id-1) *6
    uint8_t btn = (trxMsg.g2Buff[MSG_RX].data[0] - ((trxMsg.g2Buff[MSG_RX].id - 1) * 6)) - 1;

    // Serial.print("btn");
    //  found a button press
    if (btn < 6)
    {
        Serial.print(".DN: ");
        Serial.println(btn);
        // log pressed button
        btnState[btn] = 1;
        btnTime[btn] = millis();

        // enable button lamp / indicator
        // delay(100);
        trxMsg.setTxCmdData(PANEL_CMD_SET, PANEL_REG_LED, 1 << btn); // .02.06.10.08.00.01.cd.3b
        trxMsg.txMsgDataWait(Serial485);

        // track held down button
        if (btn < 2)
            btnHoldNum = btn + 1;
    }
    // read button press status
    else
    {
        uint8_t hold_state = trxMsg.g2Buff[MSG_RX].data[1];

        // if btnHoldNum  - check status to see it is released
        if (btnHoldNum && !hold_state)
        {
            Serial.print(".UP: ");
            Serial.println(btnHoldNum - 1);
            btnTime[btnHoldNum - 1] = 0;
            btnHoldNum = 0;
            trxMsg.setTxCmdData(PANEL_CMD_SET, PANEL_REG_LED, 0);
            trxMsg.txMsgDataWait(Serial485);
        }
        // Serial.println();
    }
    return 0;
}

int PanelManager::buttonUpdate()
{
    unsigned long tNow = millis();
    int btn;
    // bed buttons / tracking hold
#if 1
    for (btn = 0; btn < 2; btn++)
    {
        // query button pressed state
        if (btnTime[btn] && (tNow - btnTime[btn]) > BTN_POLL_MS)
        {
            // Serial.print("-X- ");
            btnTime[btn] = tNow;
            trxMsg.setTxCmdData(PANEL_CMD_GET, PANEL_REG_BTNSTATE + btn, 1);
            // trxMsg.txMsgDataWait(Serial485);
        }
    }
#endif
    // fan + lamp buttons
    for (btn = 2; btn < 6; btn++)
    {
        // a button lamp was on, need to turn off
        if (btnTime[btn] && (tNow - btnTime[btn]) > BTN_LAMP_DELAY)
        {
            Serial.print("upd.OFF: ");
            Serial.println(btn);
            trxMsg.setTxCmdData(PANEL_CMD_SET, PANEL_REG_LED, 0);
            trxMsg.txMsgDataWait(Serial485);
            btnTime[btn] = 0;
        }
    }
    return 0;
}

void PanelManager::setup()
{
    Serial485.begin(9600); // init RS485 serial port

    // init our panel cfg
    trxMsg.setTxCmdData(PANEL_CMD_SET, PANEL_REG_CONFIG, (PANEL_CFGBIT_SENDBTN));
    trxMsg.txMsgDataWait(Serial485);
    trxMsg.setTxCmdData(PANEL_CMD_SET, PANEL_REG_LED, 0);
    trxMsg.txMsgDataWait(Serial485);
}

int PanelManager::loop()
{
    int ret = 0;

    // look for incomming message data, process if completed
    if (trxMsg.rxMsgData(Serial485))
    {
        // Serial.println("RX msg_proc...");

        if (trxMsg.g2Buff[0].cmd == PANEL_CMD_GET)
        {
            buttonPress();
        }
        else if (trxMsg.g2Buff[0].cmd == PANEL_CMD_SET)
        {
            // decodeSetAddr();
        }
        trxMsg.clear_rx(); // clear RX data / ready for new message
    }
    // do somethnig else if no input
    else
    {
        buttonUpdate();
    }
    // ret = trxMsg.txMsgDataWait(Serial485);
    ret = trxMsg.txMsgData(Serial485);
    if (ret < 0)
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }

    return 0;
}

// -----------------------------------

PanelMsg trxMsg;
PanelManager aPanel;

void do_key1(); // x1310
void do_key2(); // x1008
void do_key3(); // x1003

void setup()
{
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.begin(115200);
#if 0
    Serial485.begin(9600);
    pinMode(RS485_RTS, OUTPUT);
    // Eanble RX mode
    digitalWrite(RS485_RTS, LOW);
#else
    aPanel.setup();
#endif
    delay(1000);

    Serial.println("ready...");
}

void loop()
{
    // digitalWrite(RS485_RTS, LOW);
    if (Serial.available() > 0)
    {                           // as soon as the first byte is received on Serial
        char c = Serial.read(); // read the data from serial.

        if (c == '1')
            do_key1();
        else if (c == '2')
            do_key2();
        else if (c == '3')
            do_key3();
        else
        {
            Serial.print(c);
        }
    }
#if 0
    if (trxMsg.rxMsgData(Serial485))
    {
        Serial.println("RX msg_proc...");
        trxMsg.clear();
    }
#else
    aPanel.loop();
#endif
}

// ------------------------

void do_key1()
{
    int ret = 0;

    trxMsg.clear_tx();
    trxMsg.g2Buff[MSG_TX].id = 0x02;
    trxMsg.g2Buff[MSG_TX].cmd = 0x03;      // read
    trxMsg.setAddrLen("\x13\x10", 2); // 1003 = cfg, 1008 = LED, 100B = BTN, 1310 = btn status
    trxMsg.setData("\x00\x01", 2);    // # of btn registers to read

    Serial.println("tx_cmd:1310");

    ret = trxMsg.txMsgDataWait(Serial485);
    if (ret <= 0)
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }
}

uint16_t val_1008 = 0;

void do_key2()
{
    int ret = 0;

    trxMsg.clear_tx();
    trxMsg.g2Buff[MSG_TX].id = 0x02;
    trxMsg.g2Buff[MSG_TX].cmd = 0x06;      //
    trxMsg.setAddrLen("\x10\x08", 2); // LED ring B0-5 = D0-5 LED
    // val_1008 = 0;                    // all LED off
    // val_1008 = 0xFF;                 // state LED (all) on
    // val_1008 = 0x01FF;               // state + GND LED on
    // val_1008 = 0x0003;               // D1-2 LED on /others off
    // val_1008 = 0x0100;               // GND LED on / others off
    trxMsg.g2Buff[MSG_TX].data[0] = (val_1008 & 0x0100) >> 8;
    trxMsg.g2Buff[MSG_TX].data[1] = val_1008 & 0x00FF;

    Serial.println("tx_cmd:1008");

    ret = trxMsg.txMsgDataWait(Serial485);
    if (ret <= 0)
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }
    Serial.println(val_1008, HEX);
    if (!val_1008)
        val_1008 = 1;
    else
        val_1008 = val_1008 << 1;
    if (val_1008 > 0x0100)
        val_1008 = 0;
}

uint16_t val_1003 = 0;

void do_key3()
{
    int ret = 0;

    trxMsg.clear_tx();
    trxMsg.g2Buff[MSG_TX].id = 0x02;
    trxMsg.g2Buff[MSG_TX].cmd = 0x06;      //
    trxMsg.setAddrLen("\x10\x03", 2); // 1003 = cfg, 1008 = LED, 100B = BTN, 1310 = btn status

    trxMsg.g2Buff[MSG_TX].data[0] = (val_1003 & 0x0100) >> 8;
    trxMsg.g2Buff[MSG_TX].data[1] = val_1003 & 0x00FF;

    Serial.println("tx_cmd:1003");

    ret = trxMsg.txMsgDataWait(Serial485);
    if (ret <= 0)
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }
    Serial.println(val_1003, HEX);
    if (!val_1003)
        val_1003 = 1;
    else
        val_1003 = val_1003 << 1;
    if (val_1003 > 0x80)
        val_1003 = 0;
}

// https://gist.github.com/deldrid1/3839697
// https://github.com/LacobusVentura/MODBUS-CRC16
// CRC16-modbus
uint16_t crc_chk_value(char *data_value, size_t length)
{
    uint16_t crc_value = 0xFFFF;
    int i;
    while (length--)
    {
        crc_value ^= *data_value++;
        for (i = 0; i < 8; i++)
        {
            if (crc_value & 0x0001)
            {
                crc_value = (crc_value >> 1) ^ 0xa001;
            }
            else
            {
                crc_value = crc_value >> 1;
            }
        }
    }
    return (crc_value);
}

#endif
