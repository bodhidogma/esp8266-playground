#if 0

// Demo for getting individual unified sensor data from the LPS2X series

#include <Arduino.h>

#ifdef __SAMD21G18A__
#define Serial SerialUSB
#else
#define Serial Serial
#endif

#include <Wire.h>
#include "LPS35HW.h"

void i2c_scan(void);

const uint8_t address = 0x5D;  // select different address, default is 0x5C

LPS35HW lps(address);

void setup() {
    Serial.begin(115200);
    while (!Serial)
      delay(10); // will pause Zero, Leonardo, etc until serial console opens

    Serial.println("LPS barometer low power test");

#if defined(ESP8266)
    Wire.begin(SDA, SCL);
    Wire.setClock(400000);

    if (!lps.begin(&Wire)) {  // pass already began Wire
        Serial.println("Could not find a LPS barometer, check wiring!");
        while (1) {}
    }
#else
    if (!lps.begin()) {
        Serial.println("Could not find a LPS barometer, check wiring!");
        while (1) {}
    }
#endif

    lps.setLowPower(true);
    lps.setOutputRate(LPS35HW::OutputRate_OneShot);  // get results on demand
    // lps.setLowPassFilter(LPS35HW::LowPassFilter_ODR9);  // default is off
}

void loop() {
    lps.requestOneShot();  // important to request new data before reading

    float pressure = lps.readPressure();  // hPa
    float temp = lps.readTemp();  // °C

    Serial.print("Pressure: ");
    Serial.print(pressure);
    Serial.print("hPa\ttemperature: ");
    Serial.print(temp);
    Serial.println("*C\n");

    delay(2000);
}


void i2c_scan(void)
{
  byte error, address; //variable for error and I2C address
  int nDevices;

  Serial.println("Scanning...");

  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
      nDevices++;
    }
    else if (error == 4)
    {
      Serial.print("Unknown error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");
}

#endif