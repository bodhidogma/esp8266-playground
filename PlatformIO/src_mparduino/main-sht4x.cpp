#if 0

/*************************************************** 
  This is an example for the SHTC3 Humidity & Temp Sensor

  Designed specifically to work with the SHTC3 sensor from Adafruit
  ----> https://www.adafruit.com/products/4636

  These sensors use I2C to communicate, 2 pins are required to  
  interface
 ****************************************************/

#include <Arduino.h>

#ifdef __SAMD21G18A__
#define Serial SerialUSB

#else
#define Serial Serial
#define P_SDA 21 // 21
#define P_SCL 22

#endif

#include <Wire.h>
#include "SHTSensor.h"

void i2c_probe();

//SHTSensor sht;

// To use a specific sensor instead of probing the bus use this command:
SHTSensor sht(SHTSensor::SHT4X);
// SHTSensor sht(SHTSensor::SHT3X);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  while (!Serial)
    delay(10); // will pause Zero, Leonardo, etc until serial console opens
  //delay(2000); // let serial console settle

#if 1
  Wire.begin();
  
  if (sht.init()) {
      Serial.print("init(): success\n");
  } else {
      Serial.print("init(): failed\n");
  }
  //sht.setAccuracy(SHTSensor::SHT_ACCURACY_HIGH); // only supported by >= SHT3x
  //sht.setAccuracy(SHTSensor::SHT_ACCURACY_MEDIUM); // only supported by SHT3x
  //sht.setAccuracy(SHTSensor::SHT_ACCURACY_LOW); // only supported by SHT3x

  delay(1);
  i2c_probe();
#else
  pinMode(P_SDA, OUTPUT);
  pinMode(P_SCL, OUTPUT);

  digitalWrite(P_SDA, HIGH);
  digitalWrite(P_SCL, LOW);

  digitalWrite(P_SDA, LOW);
  digitalWrite(P_SCL, HIGH);

  Serial.print("start..\n");
#endif
}

void loop() {
  // put your main code here, to run repeatedly:

  char buff[128];
#if 1
# if 1
  if (sht.readSample()) {
#if 1
      Serial.print("SHT>> RH: ");
      Serial.print(sht.getHumidity(), 2);
      Serial.print("  T:  ");
      Serial.print(sht.getTemperature(), 2);
      Serial.print("\n");
#endif
      sprintf(buff, "SHT>> RH: %d T: %d\n", (int)sht.getHumidity(), (int)sht.getTemperature());
      //Serial.print(buff);
      //sprintf(buff, "rh=%d rt=%d", sht.raw_humidity, sht.raw_temp);
      Serial.println(buff);
      
  } else {
      Serial.print("Error in readSample()\n");
  }
# endif
  i2c_probe();
  delay(2000);
#else
  digitalWrite(P_SDA, HIGH);
  digitalWrite(P_SCL, LOW);
  delay(1);

  digitalWrite(P_SDA, LOW);
  digitalWrite(P_SCL, HIGH);
  delay(1);
#endif
}

void i2c_probe()
{
  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");
 
  //delay(5000);           // wait 5 seconds for next scan
}

#endif
